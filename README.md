Robot Framework Page Object Best Practices
==============================

Introduction
------------
A guide to developing automation test framework based on [Robot Framework][Robot Framework]
and implements [Page Object][Page Object] pattern.


Code Conventions
----------------

### Tabulation

For separation columns in test cases use Spaces. Use `translate_tabs_to_spaces` setting in IDE.

### Maximum Line Length

Since Robot Framework is a Domain Specific Language, we don't need restrict the maximum line length.
Try to keep your code concise and use "..." as Line break. Example:


### Blank Lines

Separate tests and keywords with two blank lines.

### Variable naming

Use Camel case

Authors
-------

* Oleh Hordienko - Automation QA Engineer
* Gubich Eugene - QA Manager


[Robot Framework]: http://robotframework.org/
[Page Object]: https://github.com/SeleniumHQ/selenium/wiki/PageObjects
[Python PEP8]: http://www.python.org/dev/peps/pep-0008/